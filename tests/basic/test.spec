Version:	1
Release:	1
URL:		http://dicey.org/vlgothic

%global	foundry	VL
%global	fontlicense	mplus and BSD
%global	fontlicenses	LICENSE*
#%%global	fontlicensesex
%global	fontdocs	README*
%global	fontdocsex	%{fontlicenses}

%global	common_description	%{expand:
VLGothic provides Japanese TrueType fonts from the Vine Linux project.
Most of the glyphs are taken from the M+ and Sazanami Gothic fonts,
but some have also been improved by the project.
}

%global	fontfamily0	VL Gothic
%global	fontsummary0	Japanese TrueType font
%global	fontpkgheader0	%{expand:
Obsoletes:	vlgothic-fonts < %{version}-%{release}
Provides:	vlgothic-fonts = %{version}-%{release}
}
%global	fonts0			VL-Gothic-Regular.ttf
#%%global	fontsex0		
%global	fontconfs0		%{SOURCE1}
#%%global	fontconfsex0
#%%global	fontappstream0		%{SOURCE3}
#%%global	fontappstreamex0
%global	fontdescription0	%{expand:
%{common_description}

This package provides the monospace VLGothic font.
}

%global	fontfamily1	VL PGothic
%global	fontsummary1	Proportional Japanese TrueType font
%global	fontpkgheader1	%{expand:
Obsoletes:	vlgothic-p-fonts < %{version}-%{release}
Provides:	vlgothic-p-fonts = %{version}-%{release}
}
%global	fonts1			VL-PGothic-Regular.ttf
#%%global	fontsex1
%global	fontconfs1		%{SOURCE2}
#%%global	fontconfsex1
#%%global	fontappstream1		%{SOURCE4}
#%%global	fontappstreamex1
%global	fontdescription1	%{expand:
%{common_description}

This package provides the monospace VLGothic font.
}

Source0:	https://ja.osdn.net/projects/vlgothic/downloads/73361/VLGothic-%{version}.tar.xz
Source1:	65-3-%{fontpkgname0}.conf
Source2:	65-2-%{fontpkgname1}.conf

%fontpkg -a

%fontmetapkg

%prep
%setup -q -n VLGothic

%build
%fontbuild -a

%install
%fontinstall -a

%check
%fontcheck -a

%fontfiles -a

%changelog
