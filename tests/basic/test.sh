#! /bin/sh

err=0

echo "" > test.log
if cmp subpkgs <(rpmspec -q test.spec) > /dev/null 2>&1; then
	echo PASS subpkgs
else
	echo FAIL subpkgs
	false
fi
